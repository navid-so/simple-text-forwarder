package com.example.navid.textforwarder;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final EditText phoneNumber = (EditText) findViewById(R.id.phone_number);
         final EditText txt = (EditText) findViewById(R.id.txt);

        final Button submit = (Button) findViewById(R.id.btn_sub);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateInput(txt.getText().toString(),phoneNumber.getText().toString())){
                    Intent sendIntent = new Intent(Intent.ACTION_VIEW, Uri.parse( "sms:" + phoneNumber.getText() ));
                    Log.i("0", "onClick: "+txt.getText());
                    sendIntent.putExtra("sms_body", txt.getText()+"");
                    startActivity(sendIntent);
                }

            }
        });

    }
    private boolean validateInput(String txt, String phoneNumber){
        if(phoneNumber.length() < 5){
            Toast.makeText(getApplicationContext(),"phone number is not valid",Toast.LENGTH_SHORT).show();
            return false;
        }
        if(txt.matches("") || phoneNumber.matches("")){
            Toast.makeText(getApplicationContext(),"Please Fill Out The Inputs ",Toast.LENGTH_SHORT).show();
            return false;
        }
        Toast.makeText(getApplicationContext(),"ha ha ",Toast.LENGTH_SHORT).show();
        return  true;

    }
}
